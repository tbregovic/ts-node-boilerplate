import { Request, Response } from 'express';

import { IHealthCheck } from '@models/health-check';

class HealthCheckService {
  handle = (req: Request, res: Response): IHealthCheck => {
    const result: IHealthCheck = {
      isServerUp: true
    };

    // throw new Error('what is happening');

    return result;
  };
}

export default HealthCheckService;
