import { Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';

import AbstractApiController from '@controllers/abstract-api-controller';
import HealthCheckService from '@services/health-check/health-check-service';
import { IHealthCheck } from '@models/health-check';
import { IErrorResponse, ISuccessfulResponse } from '@models/response';

class HealthCheckController extends AbstractApiController {
  private readonly healthCheckService: HealthCheckService = new HealthCheckService();

  handle = (req: Request, res: Response): void => {
    try {
      const healthCheckResult: IHealthCheck = this.healthCheckService.handle(req, res);

      const response: ISuccessfulResponse = {
        data: healthCheckResult
      };

      this.handleSuccess(req, res, response);
    } catch (error) {
      // TODO Error handling

      const response: IErrorResponse = {
        statusCode: HttpStatus.INTERNAL_SERVER_ERROR,
        message: error.message
      };

      this.handleError(req, res, response);
    }
  };
}

export default new HealthCheckController().handle;
