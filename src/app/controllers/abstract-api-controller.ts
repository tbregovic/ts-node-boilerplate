import { Request, Response } from 'express';

import { ISuccessfulResponse, IErrorResponse } from '@models/response';

abstract class AbstractApiController {
  handleSuccess = (req: Request, res: Response, responseData: ISuccessfulResponse) => {
    let json = {};
    if (responseData.hasOwnProperty('data')) {
      json = responseData.data;
    }

    res.json(json);
  };

  handleError = (req: Request, res: Response, responseData: IErrorResponse) => {
    res.status(responseData.statusCode);

    res.json(responseData);
  };
}

export default AbstractApiController;
