import { Router } from 'express';

import HealthCheckController from '@controllers/health-check-controller';

class StatusRouter {
  private readonly router: Router = Router();

  get getRouter(): Router {
    this.router.get('/health-check', HealthCheckController);

    return this.router;
  }
}

export default new StatusRouter().getRouter;
