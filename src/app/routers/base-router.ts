import { Router } from 'express';

import StatusRouter from '@routers/status-router';

class BaseRouter {
  private readonly router: Router = Router();
  private readonly statusRouter: Router = StatusRouter;

  get getRouter(): Router {
    this.router.use('/status', this.statusRouter);

    return this.router;
  }
}

export default new BaseRouter().getRouter;
