export interface IHealthCheck {
  isServerUp: boolean;
}
