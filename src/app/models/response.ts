export interface ISuccessfulResponse {
  data?: any;
}

export interface IErrorResponse {
  statusCode: number;
  message?: string;
  error?: any;
}
