import express from 'express';
import cors from 'cors';

import BaseRouter from '@routers/base-router';
import { server as serverConfig } from '@config/app';

class App {
  private readonly express: express.Express = express();

  constructor() {
    // Enable cors
    this.express.use(cors());
    // Support application/json type post data
    this.express.use(express.json());
    // Support application/x-www-form-urlencoded post data
    this.express.use(express.urlencoded({
      extended: false
    }));

    this.express.use('/', BaseRouter);
  }

  start(): void {
    this.express.listen(serverConfig.port, (error: Error) => {
      if (error) {
        return console.log(error);
      }

      return console.log(`Server started at localhost:${serverConfig.port}`);
    });
  }
}

export default App;
