// Package "dotenv" has to be loaded and configured first otherwise config values aren't accessible
import dotenv from 'dotenv';
dotenv.config();
import 'module-alias/register';

import Example from "@workers/example/example";

const exampleWorker = new Example();
exampleWorker.start();
