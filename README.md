# Installation instructions
1) copy ```.env.example``` to ```.env```
2) run ```npm install```

# Setting up debugging in VS Code
Copy configuration to VS Code ```launch.json``` file
```
    {
      "name": "Attach to NPM run dev",
      "type": "node",
      "request": "attach",
      "port": 9229,
      "restart": true
    }
```
Example configurtion to use with ```ts-node``` package
```
    {
      "name": "LaunchLocally",
      "type": "node",
      "request": "launch",
      "runtimeArgs": ["-r", "ts-node/register"],
      "args": [
        "/home/$user/projects/.../src/trigger.ts"
      ]
    }
```
